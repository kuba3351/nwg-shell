%global debug_package %{nil}

Version:        0.3.3
%global   forgeurl https://github.com/nwg-piotr/nwg-dock
%forgemeta

Name:           nwg-dock
Release:        1%{?dist}
Summary:        Fully configurable dock, written in Go, aimed exclusively at sway Wayland compositor.

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         nwg-dock.makefile.patch

BuildRequires:  go
BuildRequires:  git
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(cairo-gobject)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(gtk-layer-shell-0)

Requires:       (nwg-drawer or nwg-launchers)

%description
Fully configurable (w/ command line arguments and css) dock, written in Go, aimed exclusively at sway Wayland compositor.
It features pinned buttons, task buttons, the workspace switcher and the launcher button.
The latter by default starts nwg-drawer or nwggrid (application grid) - if found.
In the picture(s) below the dock has been shown together with nwg-panel.

%prep
%autosetup


%build
%__make get
%make_build build


%install
%make_install


%check


%files
%{_bindir}/*
%{_datadir}/%{name}
%license LICENSE


%changelog
* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.6-1
- Initial build
