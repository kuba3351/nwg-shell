%global debug_package %{nil}

Name:           nwg-drawer
Version:        0.3.6
Release:        3%{?dist}
Summary:        Launcher application for nwg-shell

License:        MIT
URL:            https://github.com/nwg-piotr/nwg-drawer
Source0:        https://github.com/nwg-piotr/nwg-drawer/archive/refs/tags/v%{version}/%{name}-%{version}.tar.gz
Patch0:         nwg-drawer.makefile.patch

BuildRequires:  go
BuildRequires:  git
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(cairo-gobject)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(gtk-layer-shell-0)

Suggests:       (foot or alacritty)
Suggests:       thunar


%description
This application is a part of the nwg-shell project.
Nwg-drawer is a golang replacement to the nwggrid command (a part of nwg-launchers).
It's being developed with sway in mind, but should also work with other wlroots-based Wayland compositors.
X Window System is not officially supported, but you should be able to use the drawer
on some floating window managers (tested on Openbox).

%prep
%autosetup


%build
export CGO_LDFLAGS="${LDFLAGS}"
export CGO_CFLAGS="${CFLAGS}"
export CGO_CPPFLAGS="${CPPFLAGS}"
export CGO_CXXFLAGS="${CXXFLAGS}"
export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags=-linkmode=external"
go build -o bin/%{name} *.go

%install
%make_install


%check


%files
%{_bindir}/*
%{_datadir}/%{name}
%license LICENSE
%doc README.md


%changelog
* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.6-3
- Added README.md as docs
- Changed build process

* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.6-2
- Added missing LICENSE

 
* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.6-1
- Initial build
