%global forgeurl https://github.com/jovanlanik/gtklock

Name:           gtklock
Version:        2.0.1
%forgemeta
Release:        2%{?dist}
Summary:        GTK-based lockscreen for Wayland.

License:        GPL
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  scdoc
BuildRequires:  make
BuildRequires:  gcc
Buildrequires:  /usr/bin/wayland-scanner
BuildRequires:  pkgconfig(pam)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(gtk+-wayland-3.0)
BuildRequires:  pkgconfig(gtk-layer-shell-0)
BuildRequires:  pkgconfig(gmodule-export-2.0)


%description
%{summary}

%prep
%autosetup


%build
%make_build PREFIX="%{_prefix}"


%install
%make_install PREFIX="%{_prefix}"
install -d %{buildroot}%{_prefix}/lib/%{name}

%check


%files
%{_bindir}/%{name}
%{_pam_confdir}/%{name}
%{_mandir}/man1/%{name}.1.gz
%{_prefix}/lib/%{name}
%license LICENSE
%doc README.md


%changelog
* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-2
- Added directory placeholder for modules

* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-1
- Initial build
