%global   forgeurl https://github.com/nwg-piotr/nwg-shell
%global   sys_name nwg_shell


Name:           nwg-shell
Version:        0.3.9
%forgemeta
Summary:        Nwg-shell meta-package, installer and updater
Release:        3%{?dist}

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         %{name}.cleaning-arch-traits.patch

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       python-gobject
Requires:       python-dasbus
Requires:       gtk3
Requires:       foot
Requires:       gnome-themes-extra
Requires:       grim
Requires:       ImageMagick
Requires:       jq
Requires:       libappindicator-gtk3
Requires:       light
Requires:       font(dejavusans)
Requires:       font(dejavusansmono)
Requires:       font(dejavuserif)
Requires:       network-manager-applet
Requires:       papirus-icon-theme
Requires:       playerctl
Requires:       PolicyKit-authentication-agent
Requires:       python-geopy
Requires:       python-yaml
Requires:       slurp
Requires:       swappy
Requires:       sway
Requires:       swayidle
Requires:       swaylock
Requires:       swaybg
Requires:       wl-clipboard
Requires:       xorg-x11-server-Xwayland
Requires:       wlsunset
Requires:       azote
Requires:       gopsuinfo
Requires:       nwg-bar
Requires:       nwg-dock
Requires:       nwg-drawer
Requires:       nwg-menu
Requires:       nwg-look
Requires:       nwg-panel
Requires:       nwg-shell-config
Requires:       nwg-shell-wallpapers
Requires:       nwg-displays
Requires:       swaync
Requires:       gtklock
Requires:       gtklock-userinfo-module
Requires:       gtklock-powerbar-module
Requires:       gtklock-playerctl-module

Recommends:     thunar

Suggests:       chromium
Suggests:       mousepad

%description
%{summary}

%prep
%forgeautosetup -p1

%build
%py3_build
for lib in scripts/*; do
  sed '1{s@^#!.*python$@#!/usr/bin/env python3@}' $lib > $lib.new &&
  touch -r $lib $lib.new &&
  mv $lib.new $lib
done

%install
%py3_install
for lib in %{buildroot}%{python3_sitelib}/%{sys_name}/*.py; do
  sed '1{\@^#!.*python@d}' $lib > $lib.new &&
  touch -r $lib $lib.new &&
  mv $lib.new $lib
done
install -D -t %{buildroot}%{_bindir} scripts/screenshot
install -D -t %{buildroot}%{_datadir}/backgrounds nwg-shell.jpg


%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{python3_sitelib}/%{sys_name}/
%{python3_sitelib}/%{sys_name}-%{version}-py%{python3_version}.egg-info/
%{_datadir}/backgrounds/*

%changelog
* Sun Jan 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.9-3
- Removed Arch Linux specific scripts
- Presets patched to remove Arch Linux specific calls

* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.9-2
- Fixed dependencies

* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.9-1
- Initial build
