%global debug_package %{nil}

Version:        0.1.2
%global forgeurl https://github.com/nwg-piotr/gopsuinfo
%forgemeta

Name:           gopsuinfo
Release:        1%{?dist}
Summary:        Displays customizable system usage info

License:        BSD
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  go
BuildRequires:  git


%description
This program is a part of the nwg-shell project.
%{summary}

%prep
%autosetup


%build
%{__make} get
export CGO_LDFLAGS="${LDFLAGS}"
export CGO_CFLAGS="${CFLAGS}"
export CGO_CPPFLAGS="${CPPFLAGS}"
export CGO_CXXFLAGS="${CXXFLAGS}"
export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags=-linkmode=external"
go build -o bin/%{name} *.go

%install
%make_install


%check


%files
%{_bindir}/*
%{_datadir}/%{name}
%license LICENSE


%changelog
* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.2-1
- Initial build
