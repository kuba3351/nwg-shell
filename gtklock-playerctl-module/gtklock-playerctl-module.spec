%global forgeurl https://github.com/jovanlanik/gtklock-playerctl-module

Name:           gtklock-playerctl-module
Version:        2.0.1
%forgemeta
Release:        1%{?dist}
Summary:        gtklock module adding media player controls to the lockscreen

License:        GPL
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  scdoc
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(gmodule-export-2.0)
BuildRequires:  pkgconfig(playerctl)
BuildRequires:  pkgconfig(libsoup-2.4)

Requires:       gtklock

Supplements:    gtklock

%description
%{summary}

%prep
%autosetup


%build
%make_build PREFIX="%{_prefix}"


%install
%make_install PREFIX="%{_prefix}"


%check


%files
%{_prefix}/lib/gtklock/*
%license LICENSE
%doc README.md


%changelog
* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-1
- Initial build
