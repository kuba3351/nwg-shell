# -*-Mode: rpm-spec -*-

%global   forgeurl https://github.com/nwg-piotr/nwg-shell-config
%global sys_name nwg_shell_config

Name:           nwg-shell-config
Version:        0.4.10
%forgemeta
Summary:        Nwg-shell-config allows to set the common sway preferences
Release:        2%{?dist}

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       python-gobject
Requires:       python-i3ipc
Requires:       python-geopy

Recommends:     gtklock
Recommends:     playerctl

%description
Nwg-shell-config allows to set the common sway preferences (screen, input devices,
idle and lock screen configuration, main applications key bindings),
as well as switch between predefined desktop styles.
Each of the above may be adjusted to user’s taste, that includes placement of the application drawer,
dock, exit menu, and notification center.

%prep
%forgesetup -a

%build
%py3_build

%install
%py3_install
for lib in %{buildroot}%{python3_sitelib}/%{sys_name}/*.py; do
 sed '1{\@^#!/usr/bin/env python@d}' $lib > $lib.new &&
 touch -r $lib $lib.new &&
 mv $lib.new $lib
done
install -D -t %{buildroot}%{_datadir}/pixmaps nwg-shell-config.svg
install -D -t %{buildroot}%{_datadir}/pixmaps nwg-shell-update.svg
install -D -t %{buildroot}%{_datadir}/applications nwg-shell-config.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{python3_sitelib}/%{sys_name}/
%{python3_sitelib}/%{sys_name}-%{version}-py%{python3_version}.egg-info/
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%changelog
* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.10-2
* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.10-1
- Initial build
