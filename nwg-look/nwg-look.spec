%global debug_package %{nil}

%global forgeurl https://github.com/nwg-piotr/nwg-look

Name:           nwg-look
Version:        0.1.4
%forgemeta
Release:        3%{?dist}
Summary:        Nwg-look is a GTK3 settings editor, designed to work properly in wlroots-based Wayland environment.

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  go
BuildRequires:  git
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(cairo-gobject)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(pangocairo)

Recommends:     xcur2png


%description
Nwg-look is a GTK3 settings editor, designed to work properly in wlroots-based Wayland environment.
The look and feel is strongly influenced by LXAppearance, but nwg-look is intended to free the user from a few inconveniences:
  - It works natively on Wayland. You no longer need Xwayland, nor strange env variables for it to run.
  - It applies gsettings directly, with no need to use workarounds.
    You don't need to set gsettings in the sway config file. You don't need the import-gsettings script.


%prep
%autosetup


%build
export CGO_LDFLAGS="${LDFLAGS}"
export CGO_CFLAGS="${CFLAGS}"
export CGO_CPPFLAGS="${CPPFLAGS}"
export CGO_CXXFLAGS="${CXXFLAGS}"
export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags=-linkmode=external"
go build -o bin/%{name} *.go


%install
%make_install


%check


%files
%{_bindir}/*
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.svg
%license LICENSE


%changelog
* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.4-3
- Changed build process

* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.4-2
- Added missing LICENSE

* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.4-1
- Initial build
