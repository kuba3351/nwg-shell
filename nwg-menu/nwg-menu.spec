%global debug_package %{nil}

Version:        0.1.1
%global forgeurl https://github.com/nwg-piotr/nwg-menu
%forgemeta

Name:           nwg-menu
Release:        1%{?dist}
Summary:        This code provides the MenuStart plugin to nwg-panel. It also may be used standalone, however, with a little help from command line arguments.

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         %{name}.makefile.patch

BuildRequires:  go
BuildRequires:  git
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(gtk-layer-shell-0)

Supplements:    nwg-panel

%description
The nwg-menu command displays the system menu with simplified freedesktop main categories (8 instead of 13).
It also provides the search entry, to look for installed application on the basis of .desktop files,
and for files in XDG user directories.

You may pin applications by right-clicking them. Pinned items will appear above the categories list.
Right-click a pinned item to unpin it. The pinned items cache is shared with the nwggrid command,
which is a part of nwg-launchers.

%prep
%autosetup


%build
%{__make} get
%make_build build


%install
%make_install


%check


%files
%{_bindir}/*
%{_datadir}/%{name}
%license LICENSE


%changelog
* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.1-1
- Initial build
