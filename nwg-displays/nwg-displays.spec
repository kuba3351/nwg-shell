# -*-Mode: rpm-spec -*-

# Use 0 for release and 1 for git
Version:        0.1.4
%global   forgeurl https://github.com/nwg-piotr/nwg-displays
%forgemeta

%global sys_name nwg_displays

Name:           nwg-displays
Summary:        This program provides an intuitive GUI to manage multiple displays
Release:        1%{?dist}

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       python-gobject
Requires:       python-i3ipc
Requires:       gtk-layer-shell

%description
This program provides an intuitive GUI to manage multiple displays,
save outputs configuration and workspace-to-output assignments.

%prep
%forgesetup -a

%build
%py3_build

%install
%py3_install
for lib in %{buildroot}%{python3_sitelib}/%{sys_name}/*.py; do
 sed '1{\@^#!/usr/bin/env python@d}' $lib > $lib.new &&
 touch -r $lib $lib.new &&
 mv $lib.new $lib
done
install -D -t %{buildroot}%{_datadir}/pixmaps nwg-displays.svg
install -D -t %{buildroot}%{_datadir}/applications nwg-displays.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{python3_sitelib}/%{sys_name}/
%{python3_sitelib}/%{sys_name}-%{version}-py%{python3_version}.egg-info/
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%changelog
* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.4-1
- Initial build
