%global debug_package %{nil}
%global forgeurl  https://github.com/nwg-piotr/nwg-shell-wallpapers

Name:           nwg-shell-wallpapers
Version:        1.1
%forgemeta
Release:        1%{?dist}
Summary:        nwg-shell wallpapers package
BuildArch:      noarch
License:        CC0-1.0
URL:            %{forgeurl}
Source0:        %{forgesource}

%description
%{summary}

%prep
%forgeautosetup


%build


%install
install -d %{buildroot}%{_datadir}/backgrounds/nwg-shell
install -Dm644 wallpapers/* %{buildroot}%{_datadir}/backgrounds/nwg-shell

%check


%files
%license LICENSE
%dir %{_datadir}/backgrounds/nwg-shell
%{_datadir}/backgrounds/nwg-shell/*


%changelog
* Sat Dec 31 2022 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.1-1
- Initial build
