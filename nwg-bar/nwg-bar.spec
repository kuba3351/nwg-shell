%global debug_package %{nil}

Version:        0.1.0
%global forgeurl https://github.com/nwg-piotr/nwg-bar
%forgemeta

Name:           nwg-bar
Release:        1%{?dist}
Summary:        nwg-bar is a Golang replacement to the nwgbar command (a part of nwg-launchers), with some improvements. Aimed at sway, works with wlroots-based compositors only.

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         %{name}.makefile.patch

BuildRequires:  go
BuildRequires:  git
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(gtk-layer-shell-0)

Supplements:    nwg-panel

%description
The nwg-bar command creates a button bar on the basis of a JSON template placed in the ~/.config/nwg-bar/ folder.
By default the command displays a horizontal bar in the center of the screen.
Use command line arguments to change the placement.
%prep
%autosetup


%build
%{__make} get
%make_build build


%install
%make_install


%check


%files
%{_bindir}/*
%{_datadir}/%{name}
%license LICENSE


%changelog
* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.0-1
- Initial build
